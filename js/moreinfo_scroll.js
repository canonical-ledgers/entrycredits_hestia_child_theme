jQuery(document).ready(function ($) {
    $('a.btn.btn-primary.btn-lg').click(function (e) {
        e.preventDefault();

        $('html,body').animate({
            scrollTop: $('section#features').offset().top - $('nav.navbar').height() - 10},
            'slow'
        );
        
    });
});
