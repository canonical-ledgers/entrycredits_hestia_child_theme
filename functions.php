<?php
function entrycredits_hestia_child_frontscripts() {
	wp_enqueue_style('hestia-style', get_template_directory_uri() . '/style.css', array());
	wp_enqueue_style('entrycredits-hestia-style', get_stylesheet_directory_uri() . '/style.css', array(), wp_get_theme()->get('Version'));
    wp_enqueue_script("jquery");
    wp_enqueue_style('thankyou_styles', get_stylesheet_directory_uri() . '/css/thankyou.css', array(), false);

    if (is_page('checkout')) {
        wp_enqueue_script('entrycredits_checkout', get_stylesheet_directory_uri() . '/js/cleanup_woocommerce_fields.js', array(), false);
    }

    if (is_front_page()) {
        wp_enqueue_script('moreinfo_scroll', get_stylesheet_directory_uri() . '/js/moreinfo_scroll.js', array(), false);
    }
}
add_action( 'wp_enqueue_scripts', 'entrycredits_hestia_child_frontscripts' );

function remove_bottom_footer_content() {
    remove_action('hestia_do_bottom_footer_content', 'bottom_footer_content', 0);
}
add_action('init', 'remove_bottom_footer_content');

function entrycredits_bottom_footer_content () {
    wp_nav_menu(
        array(
            'theme_location' => 'footer',
            'depth'          => 1,
            'container'      => 'ul',
            'menu_class'     => 'footer-menu',
        )
    );
    if ( ! empty( $hestia_general_credits ) || is_customize_preview() ) : ?>
        <div class="copyright <?php echo esc_attr( $this->add_footer_copyright_alignment_class() ); ?>">
        </div>
        <?php
    endif;
}
add_action('bottom_footer_content', 'entrycredits_bottom_footer_content');

// removes advertisting for parent theme
function remove_parent_sections ($wp_customize) {
    $wp_customize->remove_section('hestia_upsell_main_section');
    $wp_customize->remove_section('hestia_front_page_sections_upsell_section');
}
add_action("customize_register", "remove_parent_sections", 100);
