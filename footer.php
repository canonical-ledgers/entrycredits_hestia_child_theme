<footer class="footer footer-black footer-big">
    <?php wp_nav_menu(array("menu" => 'navbar-additional', "container" => '', "menu_class" => 'additional-nav pull-left')); ?>
    <div class="copyright pull-right">
        Copyright 2018 <a href="https://www.canonical-ledgers.com">Canonical Ledgers, LLC</a>
    </div>
</footer>
</div></div>
<?php wp_footer(); ?>
</body>
</html>
